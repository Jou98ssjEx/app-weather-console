const inquirer = require('inquirer');
const colors = require('colors');



const mainMenu = async() => {

    console.clear();

    console.log('======================='.green);
    console.log(colors.bold('        Welcome '.blue));
    console.log('=======================\n'.green);

    const questions = [{
        type: 'list',
        name: 'options',
        message: 'Choose an option',
        choices: [{
                value: 1,
                name: `${'1.'.green} ${'Search city'}`
            },
            {
                value: 2,
                name: `${'2.'.green} ${'History of cities'}`
            },
            {
                value: 0,
                name: `${'0.'.green} ${' exit'}`
            },
        ]
    }];
    const { options } = await inquirer.prompt(questions);

    return options;
}

const pause = async() => {

    const question = [{
        type: 'input',
        name: 'continue',
        message: ` Press ${'Enter'.green} to continue `
    }];
    console.log();
    await inquirer.prompt(question);
}

const readInput = async(message) => {
    const questions = [{
        type: 'intpu',
        name: 'info',
        message,
        validate(value) {
            if (value.length === 0) {
                return 'Ingrese una descripcion'
            }
            return true;
        }
    }];

    const { info } = await inquirer.prompt(questions);
    return info;
}

const listSities = async(sities = []) => {

    const choices = sities.map((site, id) => {
        const idx = `${id +1}.`.green;
        return {
            value: site.id,
            name: `${idx} ${site.name}`
        }
    });

    choices.unshift({
        value: 0,
        name: `${'0'.green} ${'Cancel'}`
    });

    const questions = [{
        type: 'list',
        name: 'options',
        message: 'Choose an option',
        choices
    }];

    const { options } = await inquirer.prompt(questions);

    return options;

}

module.exports = {
    mainMenu,
    pause,
    readInput,
    listSities
    // info
}