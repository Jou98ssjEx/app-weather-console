const axios = require('axios');
const fs = require('fs');
const path = './db/data.json';


class Searches {
    historial = [];

    constructor() {

        this.readDB();
    }

    get historialCapi() {

        // return this.historial.forEach(e => {
        //     console.log(e[0].toUpperCase() + e.slice(1));
        // })
        return this.historial.map(e => {
            // e[0].toUpperCase() + e.slice(1);
            let part = e.split(' ');
            part = part.map(p => p[0].toUpperCase() + p.substring(1));

            return part.join(' ');

            // console.log(e.splip(' '));
            // part[0].toUpperCase();
        })
    }
    get paramsMapbox() {
        return {
            'access_token': process.env.MAPBOX_KEY,
            'limit': 5,
            'language': 'es'
        }
    }

    get paramsOpenWeather() {
        return {
            'appid': process.env.OPENWEATHER_KEY,
            'lang': 'es',
            'units': 'metric'
        }
    }

    async searchCity(site = '') {

        try {

            const instance = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${site}.json`,
                params: this.paramsMapbox

            });

            const resp = await instance.get();

            return resp.data.features.map(site => ({

                id: site.id,
                name: site.place_name,
                lng: site.center[0],
                lat: site.center[1]

            }));

        } catch (error) {
            throw 'Error en la peticion'
        }

    }

    async weatherSite(lat, lon) {
        try {
            const instance = axios.create({
                baseURL: `https://api.openweathermap.org/data/2.5/weather`,
                params: {...this.paramsOpenWeather, lat, lon }
                // baseURL: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}`,
                // params: this.paramsOpenWeather

            });

            const { data } = await instance.get();

            const { weather, main } = data;

            // return data;
            return {
                desc: weather[0].description,
                temp: main.temp,
                max: main.temp_max,
                min: main.temp_min
            }

        } catch (error) {
            throw 'Error en la peticion'
        }
    }

    addHistorial(site = '') {

        if (this.historial.includes(site.toLowerCase())) {
            return
        }
        this.historial = this.historial.splice(0, 5);
        // site.toUpperCase

        this.historial.unshift(site.toLowerCase());

        this.savedDB();
    }

    savedDB() {
        const payload = {
            historial: this.historial
        }
        fs.writeFileSync(path, JSON.stringify(payload));
    }

    readDB() {

        if (!fs.existsSync(path)) {
            console.log('NO Existe');
            return null;
        }

        const info = fs.readFileSync(path, { encoding: 'utf-8' });
        console.log({ info });
        const data = JSON.parse(info);
        console.log({ data });

        this.historial = data.historial;
        // return data;
    }
}

module.exports = Searches;