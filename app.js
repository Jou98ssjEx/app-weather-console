require('dotenv').config();
const { mainMenu, pause, readInput, listSities } = require("./helpers/inquirer");
const Searches = require("./models/searches");

console.clear();

const main = async() => {

    const searches = new Searches();
    let options;

    // const a = searches.readDB();

    do {

        options = await mainMenu();

        switch (options) {
            case 1:

                const nameCity = await readInput('City: ');

                const cities = await searches.searchCity(nameCity);
                // console.log(arrCity);

                const city = await listSities(cities);
                if (city === 0) {
                    console.log('Regresa al menu');
                } else {

                    const citySel = cities.find(l => l.id === city);
                    // console.log(citySel);
                    searches.addHistorial(citySel.name);
                    const { desc, temp, max, min } = await searches.weatherSite(citySel.lat, citySel.lng);

                    console.clear();
                    console.log('\nInformation of the City\n'.green);
                    console.log(' City:', citySel.name);
                    console.log(' Lat:', citySel.lat);
                    console.log(' Lng:', citySel.lng);
                    console.log(' Temperature:', temp);
                    console.log(' Max:', max);
                    console.log(' Min:', min);
                    console.log(' Clima: ', desc);
                }


                break;
            case 2:

                console.log('\n');
                // const a = searches.historial.forEach((e, i) => {
                //     const idx = `${i +1}.`.green;
                //     console.log(`${idx} ${e}`);
                //     // return `${idx} ${e}`;

                // });

                searches.historialCapi.forEach((e, i) => {
                    const idx = `${i +1}.`.green;
                    console.log(`${idx} ${e}`);
                    // return `${idx} ${e}`;

                });
                // console.log(a);
                break;
            case 0:

                break;

        }

        if (options !== 0)
            await pause();

    } while (options !== 0);


}

main();